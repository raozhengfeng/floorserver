/*
 * File: SocketManager.h
 * Author: raozf
 *
 * Created on 2013年3月11日, 下午5:32
 */

#ifndef SOCKETMANAGER_H
#define SOCKETMANAGER_H

#include "Common.h"
#include "Runable.h"
namespace Floor
{

    class SocketManager : public Runable
    {
    private:
        boost::unordered_map<int, time_t> m_SocketMap;
        boost::mutex m_Mutex;
        typedef boost::unordered_map<int, time_t>::iterator iterator;

    public:

        void UpdateActiveTime(int sock_fd)
        {
            time_t _now = time(NULL);

            LOCK _l(m_Mutex);
            iterator it = m_SocketMap.find(sock_fd);
            if (it == m_SocketMap.end())
            {
                m_SocketMap.insert(std::pair<int, time_t>(sock_fd, _now));
                LOG(LOG_LEVEL_DEBUG, "[fd:" << sock_fd << "] inseret, time:" << _now);
            }
            else
            {
                it->second = _now;
                LOG(LOG_LEVEL_DEBUG, "[fd:" << sock_fd << "] update , time:" << _now);
            }
        }

        //    void Run()
        //    {
        //        while ( m_bExit == false )
        //        {
        //            time_t _now = time(NULL);
        //            pthread_mutex_lock(&m_Mutex);
        //
        //            BOOST_FOREACH(iterator::value_type& it, m_SocketMap)
        //            {
        //                if (_now - it.second >= SOCK_INACTIVE_TIMEOUT)
        //                {
        //                    close(it.first);
        //                    //LOG_NODE("[fd:"<< it.first<<"] inactive timeout, force closed. update time: "<<it.second);
        //
        //                    //m_sockets.erase(it.first);
        //                    //unnecessary to remove it.
        //                    //the socket handle will be used for other sockets.
        //                }
        //            }
        //            pthread_mutex_unlock(&m_Mutex);
        //
        //            sleep(2);
        //        }
        //    }
    };

}
#endif /* SOCKETMANAGER_H */

