/*
 * File:   Object.h
 * Author: raozf
 *
 * Created on 2014年8月27日, 下午6:13
 */

#ifndef OBJECT_H
#define	OBJECT_H

#include "Common.h"
namespace Floor
{

    class Object
    : public boost::noncopyable
    {
    private:
        std::string m_strName;
        bool m_bCreated;
    protected:
        virtual bool t_Create() = 0;
        virtual void t_Destory() = 0;

    public:

        Object(const std::string& strName)
        : m_strName(strName)
        , m_bCreated(false) { }

        virtual ~Object()
        {
            //do not call pure virtual function in decontructor!
            //see http://www.cnblogs.com/chutianyao/p/3973237.html
            //Destory();
        }

        bool Create()
        {
            if (m_bCreated == false)
            {
                LOG(LOG_LEVEL_NODE, "[Object]Creating " << m_strName << "(" << this << ")...");

                m_bCreated = t_Create();
                if (m_bCreated == false)
                {
                    LOG(LOG_LEVEL_FATAL, "[Object]" << m_strName << "(" << this << ") create failed.");
                }
                else
                {
                    LOG(LOG_LEVEL_NODE, "[Object]" << m_strName << "(" << this << ") created.");
                }
            }
            else
            {
                LOG(LOG_LEVEL_WARN, "[Object]" << m_strName << "(" << this << ") has been created already.");
            }

            return m_bCreated;
        }

        void Destory()
        {
            if (m_bCreated == true)
            {
                LOG(LOG_LEVEL_NODE, "[Object]Destory " << m_strName << "(" << this << ")...");
                t_Destory();
                LOG(LOG_LEVEL_NODE, "[Object]" << m_strName << "(" << this << ") destoryed.");

                m_bCreated = false;
            }
            else
            {
                LOG(LOG_LEVEL_WARN, "[Object]" << m_strName << "(" << this << ") has been destoryed already.");
            }
        }
    };

}
#endif	/* OBJECT_H */
