/* 
 * File:   BanEnginee.h
 * Author: raozf
 *
 * Created on 2014年8月22日, 下午1:49
 */

#ifndef BANENGINEE_H
#define	BANENGINEE_H
namespace Floor
{

    class BanEnginee
    {
    public:

        BanEnginee() { }

        virtual ~BanEnginee() { }

        virtual bool Start() { }

        virtual void Stop() { }
    };

}
#endif	/* BANENGINEE_H */

