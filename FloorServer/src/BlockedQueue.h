/*
 * File: BlockedQueue.h
 * Author: chu
 *
 * Created on 2013年3月9日, 下午4:53
 */

#ifndef BLOCKEDQUEUE_H
#define BLOCKEDQUEUE_H

#include "Common.h"
namespace Floor
{

    template<typename T>
    class BlockedQueue
    {
    private:
        int m_nCapacity;
        std::queue<T> m_Elements;

        boost::mutex m_Mutex;
        boost::condition m_Condition;

    public:

        BlockedQueue(int capacity = -1)
        : m_nCapacity(capacity) { }

        void Push(T element)
        {
            LOCK _l(m_Mutex);

            if ((m_nCapacity > 0) && (m_Elements.size() >= m_nCapacity))//connection is full. Reject it!
            {
                LOG(LOG_LEVEL_WARN, "************ TaskQueue full! ************ capacity = " << m_nCapacity
                    << ", size = " << m_Elements.size()
                    //<< ", fd = " << fd
                    );
                //close(fd); //*********************

                return;
            }

            m_Elements.push(element);
            m_Condition.notify_one();
        }

        //will block if no elements

        T Pop()
        {
            T _element;
            LOCK _l(m_Mutex);

            /*
            shall be called with mutex locked by the calling thread or undefined behavior results.
            thread_cond_wait(pthread_cond_t *cond, pthread_mutex_t *mutex)函数传入的参数mutex用于保护条件，
            因为我们在调用pthread_cond_wait时，如果条件不成立我们就进入阻塞，但是进入阻塞这个期间，如果条件变量改变了的话，
            那我们就漏掉了这个条件。因为这个线程还没有放到等待队列上，所以调用pthread_cond_wait前要先锁互斥量，
            即调用pthread_mutex_lock(),pthread_cond_wait在把线程放进阻塞队列后，自动对mutex进行解锁，
            使得其它线程可以获得加锁的权利。这样其它线程才能对临界资源进行访问并在适当的时候唤醒这个阻塞的进程。
            当pthread_cond_wait返回的时候又自动给mutex加锁,所以最后我们要手动解锁。
             */
            //no elements, need to wait.
            if (m_Elements.size() <= 0)
            {
                m_Condition.wait(_l);
                _l.unlock();
                return Pop();
            }
            else
            {
                _element = m_Elements.front();
                m_Elements.pop();
            }

            return _element;
        }

        //timed block if no element

        int Pop_Timed()
        {
            T _element;
            LOCK _l(m_Mutex);

            //no elements, need to wait for some time.
            if (m_Elements.size() <= 0)
            {
                m_Condition.timed_wait(_l, boost::get_system_time() + boost::posix_time::seconds(3));
                _l.unlock();
            }
            else
            {
                _element = m_Elements.front();
                m_Elements.pop();
            }

            return _element;
        }
    };

    typedef BlockedQueue<int> SocketQueue;

}
#endif /* BLOCKEDQUEUE_H */