/*
 * File: Listener.h
 * Author: raozf
 *
 * Created on 2013年3月19日, 上午11:00
 */

#ifndef LISTENER_H
#define LISTENER_H

#include "Common.h"
#include "Object.h"
#include "Task.h"
#include "SocketManager.h"
namespace Floor
{

    typedef boost::function<void(int) > NewConnectionCallback;

    class Listener
    : public Object
    , public Task
    {
    private:
        int m_nListenFD;
        uint16_t m_nServerPort;
        uint16_t m_nListenerMaxEvents;

        NewConnectionCallback m_NewConnectionCallback;

    private:
        bool m_SetNonBlocking(int nFD)
        {
            int _nOpts;
            _nOpts = fcntl(nFD, F_GETFL);
            if (_nOpts < 0)
            {
                LOG(LOG_LEVEL_ERROR, "fcntl(fd, F_GETFL) returned:" << _nOpts << ", fd = " << nFD);
                return false;
            }

            _nOpts = _nOpts | O_NONBLOCK;
            if (fcntl(nFD, F_SETFL, _nOpts) < 0)
            {
                LOG(LOG_LEVEL_ERROR, "fcntl(fd, F_SETFL, opts) failed. opts = " << _nOpts << ", fd = " << nFD);
                return false;
            }

            return true;
        }

        bool m_ReuseSocket(int nFD)
        {
            int _nReuse = 1;
            if (setsockopt(nFD, SOL_SOCKET, SO_REUSEADDR, &_nReuse, sizeof (_nReuse)) == -1)
            {
                LOG(LOG_LEVEL_ERROR, "setsockopt(SO_REUSEADDR) failed. " << ", fd = " << nFD);
                return false;
            }

            return true;
        }

        virtual bool t_Create()
        {
            m_nListenFD = socket(AF_INET, SOCK_STREAM, 0);
            if ((m_ReuseSocket(m_nListenFD) == false)
                || (m_SetNonBlocking(m_nListenFD) == false))
            {
                goto _ERROR;
            }

            struct sockaddr_in _nServerAddress;
            bzero(&_nServerAddress, sizeof (_nServerAddress));
            _nServerAddress.sin_family = AF_INET;
            _nServerAddress.sin_port = htons(m_nServerPort);
            _nServerAddress.sin_family = INADDR_ANY;
            if (bind(m_nListenFD, (sockaddr*) & _nServerAddress, sizeof (_nServerAddress)) < 0)
            {
                LOG(LOG_LEVEL_FATAL, "bind() failed. port:" << m_nServerPort << ", " << strerror(errno));
                goto _ERROR;
            }

            if (listen(m_nListenFD, LISTENQ) < 0)
            {
                LOG(LOG_LEVEL_FATAL, "listen() failed. port:" << m_nServerPort << ", " << strerror(errno));
                goto _ERROR;
            }

            LOG(LOG_LEVEL_NODE, "Listening port: " << m_nServerPort << "...");
            return true;

_ERROR:
            t_Destory();
            return false;
        }

        virtual void t_Destory()
        {
            CLOSE_SOCKET(m_nListenFD);
        }

    protected:

        /*
         * accept new connection
         * deliver the new connection to source enginee, the source enginee will assign it to proper follower
         */
        virtual void t_Run()
        {
            LOG(LOG_LEVEL_DEBUG, "Listener::t_Run().");

            int _nEpollFD = -1;
            int _nConnectedFD = -1;
            struct epoll_event _Ev, _Events[m_nListenerMaxEvents];
            struct sockaddr_in _ClientAddress;
            socklen_t _Clilent = sizeof (_ClientAddress);

            if (m_nListenFD < 0)
            {
                LOG(LOG_LEVEL_FATAL, "Invalid socket: " << m_nListenFD);
                goto _ERROR;
            }

            /* The size is not the maximum size of the backing store but just a hint to the kernel about how to dimension internal structures.
             * (Nowadays, size is ignored. Since Linux 2.6.8, the size argument is unused:The kernel dynamically sizes the required data structures
             * without needing this initial hint.)*/
            _nEpollFD = epoll_create(256);
            _Ev.data.fd = m_nListenFD;
            _Ev.events = EPOLLIN;
            epoll_ctl(_nEpollFD, EPOLL_CTL_ADD, m_nListenFD, &_Ev);

            while (m_bExit == false)
            {
                if (epoll_wait(_nEpollFD, _Events, m_nListenerMaxEvents, 1000) > 0)
                {
                    memset(&_ClientAddress, 0, sizeof (struct sockaddr_in));
                    _nConnectedFD = accept(m_nListenFD, (sockaddr*) & _ClientAddress, &_Clilent);
                    if (_nConnectedFD < 0)
                    {
                        LOG(LOG_LEVEL_ERROR, "accept() returned fd:" << _nConnectedFD << ". error msg:" << strerror(errno));
                        continue;
                    }

                    if (m_SetNonBlocking(_nConnectedFD) == false)
                    {
                        CLOSE_SOCKET(_nConnectedFD);
                        LOG(LOG_LEVEL_WARN, "[fd:" << _nConnectedFD << "] SetNonBlocking() failed. close socket.");
                        continue;
                    }

                    //The string is returned in a statically allocated buffer, which subsequent calls will overwrite.
                    //which means:we don't need to free the string returnd by inet_ntoa(), but it's non-re-entry,be careful in multithreads.
                    char *_pClientIP = inet_ntoa(_ClientAddress.sin_addr);
                    LOG(LOG_LEVEL_NODE, "[fd:" << _nConnectedFD << "] connect from:" << _pClientIP);
                    //TODO: record remote peer ip & port, limit connections of per ip

                    //deliver the socket to source enginee
                    m_NewConnectionCallback(_nConnectedFD);
                }
            }

_ERROR:
            LOG(LOG_LEVEL_DEBUG, "Listener::t_Run() exited.");
        }

    public:

        Listener(const std::string & strName, uint16_t nServerPort, uint16_t nListenerMaxEvents, NewConnectionCallback callback)
        : Object(strName)
        , Task(strName)
        , m_nServerPort(nServerPort)
        , m_nListenerMaxEvents(nListenerMaxEvents)
        , m_NewConnectionCallback(callback)
        , m_nListenFD(-1)
        {
            ASSERT(m_nServerPort > 0);
            ASSERT(m_nListenerMaxEvents > 0);
        }

        virtual ~Listener(){}
    };

}
#endif /* LISTENER_H */
