/*
 * File:   FollowerManager.h
 * Author: raozf
 *
 * Created on 2014年8月22日, 下午5:58
 */

#ifndef FOLLOWERMANAGER_H
#define	FOLLOWERMANAGER_H

#include "Common.h"
#include "FollowerGroup.h"
namespace Floor
{

    class FollowerManager
    : public Object
    , public Runable
    {
    private:
        size_t m_nFollowerGroups;
        size_t m_nFollowersPerGroup;
        size_t m_nRobinIndex;
        std::vector<FollowerGroup*> m_FollowerGroups;

        ConnectCallback m_ConnectCallback;
        RecvCallback m_RecvCallback;
    protected:
        virtual bool t_Create()
        {
            for (size_t i = 0; i < m_nFollowerGroups; i++)
            {
                FollowerGroup* _p = new FollowerGroup("FollowerGroup", m_nFollowersPerGroup, m_ConnectCallback, m_RecvCallback);
                if (_p == NULL)
                {
                    LOG(LOG_LEVEL_FATAL, "Out of memory! New FollowerGroup failed.");
                    goto _ERROR;
                }
                m_FollowerGroups.push_back(_p);

                if (_p->Create() == false)
                {
                    LOG(LOG_LEVEL_FATAL, "FollowerGroup(" << _p << ") Create() failed.");
                    goto _ERROR;
                }
            }
            return true;

_ERROR:
            t_Destory();
            return false;
        }

        virtual void t_Destory()
        {
            BOOST_FOREACH(FollowerGroup* _pFollowerGroup, m_FollowerGroups)
            {
                SAFE_DESTORY(_pFollowerGroup);
            }
            m_FollowerGroups.clear();
        }

        virtual bool t_Start()
        {
            BOOST_FOREACH(FollowerGroup* _pFollowerGroup, m_FollowerGroups)
            {
                if (_pFollowerGroup != NULL)
                {
                    if (_pFollowerGroup->Start() == false)
                    {
                        LOG(LOG_LEVEL_FATAL, "FollowerGroup (" << _pFollowerGroup << ") Start() failed.");
                        goto _ERROR;
                    }
                }
            }

            return true;

_ERROR:
            t_Stop();
            return false;
        }

        virtual void t_Stop()
        {
            BOOST_FOREACH(FollowerGroup* _pFollowerGroup, m_FollowerGroups)
            {
                if (_pFollowerGroup != NULL)
                {
                    _pFollowerGroup->Stop();
                }
            }
        }

    public:
        FollowerManager(const std::string& strName,
                size_t nFollowerGroups,
                size_t nFollowersPerGroup,
                ConnectCallback OnConnectCallback,
                RecvCallback OnRecvCallback)
        : Object(strName)
        , Runable(strName)
        , m_nFollowerGroups(nFollowerGroups)
        , m_nFollowersPerGroup(nFollowersPerGroup)
        , m_ConnectCallback(OnConnectCallback)
        , m_RecvCallback(OnRecvCallback)
        , m_nRobinIndex(0)
        {
            ASSERT(m_nFollowerGroups > 0);
            ASSERT(m_nFollowersPerGroup > 0);
        }

        virtual ~FollowerManager() { }

        void DispatchConnection(int nSocket)
        {
            m_FollowerGroups[m_nRobinIndex++]->DispatchConnection(nSocket);
            m_nRobinIndex %= m_nFollowerGroups;
        }
    };

}
#endif	/* FOLLOWERMANAGER_H */
