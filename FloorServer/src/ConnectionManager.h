/*
 * File:   ConnectionManager.h
 * Author: chu
 *
 * Created on 2014年9月21日, 下午8:51
 */

#ifndef CONNECTIONMANAGER_H
#define	CONNECTIONMANAGER_H

#include "Object.h"
#include "Connection.h"

namespace Floor
{

    class ConnectionManager
    : public Object
    {
    private:
        std::map<int, Connection*> m_Connections;
        boost::mutex m_Mutex;

    protected:
        virtual bool t_Create()
        {
            return true;
        }

        virtual void t_Destory()
        {
             return;
        }

    public:
        void AddConnection(int nFD)
        {
            LOCK _l(m_Mutex);
        }

        Connection* GetConnection(int nFD)
        {

        }

    };

}
#endif	/* CONNECTIONMANAGER_H */

