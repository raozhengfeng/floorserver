/*
 * File: Runable.h
 * Author: raozf
 *
 * Created on 2013年3月19日, 上午11:08
 */

#ifndef RUNABLE_H
#define RUNABLE_H
namespace Floor
{

    class Runable
    {
    private:
        bool m_bStarted;
    protected:
        std::string m_strName;
        virtual bool t_Start() = 0;
        virtual void t_Stop() = 0;

    public:

        Runable(const std::string& strName)
        : m_strName(strName)
        , m_bStarted(false) { }

        virtual ~Runable()
        {
        }

        bool Start()
        {
            if (m_bStarted == false)
            {
                LOG(LOG_LEVEL_DEBUG, "Starting " << m_strName << "(" << this << ")...");

                m_bStarted = t_Start();
                if (m_bStarted == false)
                {
                    LOG(LOG_LEVEL_FATAL, m_strName << "(" << this << ") start failed.");
                }
                else
                {
                    LOG(LOG_LEVEL_DEBUG, m_strName << "(" << this << ") started.");
                }
            }
            else
            {
                LOG(LOG_LEVEL_WARN, m_strName << "(" << this << ") has been started already.");
            }

            return m_bStarted;
        }

        void Stop()
        {
            if (m_bStarted == true)
            {
                LOG(LOG_LEVEL_DEBUG, "Stopping " << m_strName << "(" << this << ")...");

                t_Stop();
                m_bStarted = false;

                LOG(LOG_LEVEL_DEBUG, m_strName << "(" << this << ") stopped.");
            }
            else
            {
                //LOG(LOG_LEVEL_DEBUG, m_strName << "(" << this << ") has been stopped already.");
            }
        }
    };

}
#endif /* RUNABLE_H */

