/*
 * File:   SourceEnginee.h
 * Author: raozf
 *
 * Created on 2014年8月22日, 下午1:50
 */

#ifndef SOURCEENGINEE_H
#define	SOURCEENGINEE_H

#include "Object.h"
#include "Runable.h"
#include "Configure.h"
#include "Listener.h"
#include "FollowerManager.h"
namespace Floor
{

    /*
     * use leader-follower model of epoll with EPOLLONESHOT option,
     * please refer to http://bbs.chinaunix.net/thread-4067753-1-1.html
     */
    class SourceEnginee
    : public Object
    , public Runable
    {
    private:
        uint16_t m_nServerPort;
        SourceEngineeConfigure* m_pConfigure;
        ConnectCallback m_ConnectCallback;
        RecvCallback m_RecvCallback;

        Listener* m_pListener;
        FollowerManager* m_pFollowerManager;

    protected:
        virtual bool t_Create()
        {
            ASSERT(m_pListener == NULL);
            ASSERT(m_pFollowerManager == NULL);

            //create listener
            m_pListener = new Listener("Listener"
                , m_nServerPort
                , m_pConfigure->m_nListenerMaxEvents
                , boost::bind(&SourceEnginee::DispatchConnection, this, _1));
            if (m_pListener == NULL)
            {
                LOG(LOG_LEVEL_FATAL, "Out of memory! New Listener failed.");
                goto _ERROR;
            }

            if (m_pListener->Create() == false)
            {
                LOG(LOG_LEVEL_FATAL, "Listener Create() failed.");
                goto _ERROR;
            }

            //create follower manager
            m_pFollowerManager = new FollowerManager("FollowerManager"
                , m_pConfigure->m_nFollowerGroups
                , m_pConfigure->m_nFollowersPerGroup
                , m_ConnectCallback
                , m_RecvCallback);
            if (m_pFollowerManager == NULL)
            {
                LOG(LOG_LEVEL_FATAL, "Out of memory! New FollowerManager failed.");
                goto _ERROR;
            }

            if (m_pFollowerManager->Create() == false)
            {
                LOG(LOG_LEVEL_FATAL, "FollowerManager Create() failed.");
                goto _ERROR;
            }

            return true;

_ERROR:
            t_Destory();
            return false;
        }

        virtual void t_Destory()
        {
            SAFE_DESTORY(m_pListener);
            SAFE_DESTORY(m_pFollowerManager);
        }

        virtual bool t_Start()
        {
            ASSERT(m_pListener);
            ASSERT(m_pFollowerManager);

            if (m_pListener->Start() == false)
            {
                LOG(LOG_LEVEL_FATAL, "Listener start failed! m_pListener: " << m_pListener);
                return false;
            }

            if (m_pFollowerManager->Start() == false)
            {
                LOG(LOG_LEVEL_FATAL, "FollowerManager start failed! m_pListener: " << m_pListener);
                m_pListener->Stop();
                return false;
            }

            return true;
        }

        virtual void t_Stop()
        {
            ASSERT(m_pListener);
            ASSERT(m_pFollowerManager);

            m_pListener->Stop();
            m_pFollowerManager->Stop();
        }

    public:
        SourceEnginee(const std::string & strName,
                uint16_t nServerPort,
                SourceEngineeConfigure* pConfigure,
                ConnectCallback OnConnectCallback,
                RecvCallback OnRecvCallback)
        : Object(strName)
        , Runable(strName)
        , m_nServerPort(nServerPort)
        , m_pConfigure(pConfigure)
        , m_ConnectCallback(OnConnectCallback)
        , m_RecvCallback(OnRecvCallback)
        , m_pListener(NULL)
        , m_pFollowerManager(NULL)
        {
            ASSERT(m_pConfigure);
            ASSERT(m_pConfigure->m_nFollowerGroups > 0);
            ASSERT(m_pConfigure->m_nFollowersPerGroup > 0);
        }

        virtual ~SourceEnginee() { }

        void DispatchConnection(int nSocket)
        {
            if (m_pFollowerManager != NULL)
            {
                m_pFollowerManager->DispatchConnection(nSocket);
            }
        }
    };

}
#endif	/* SOURCEENGINEE_H */