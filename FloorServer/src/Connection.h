/*
 * File:   Connectton.h
 * Author: chu
 *
 * Created on 2014年9月21日, 下午8:31
 */

#ifndef CONNECTION_H
#define	CONNECTION_H

#include "BufferManager.h"
namespace Floor
{
    class Connection
    {
    private:
        static uint64_t m_nConnectionCounter;

        int m_nFD;
        uint64_t m_nID;
        FollowerBuffer* m_pBuffer;
    public:
        Connection(int nFD)
        : m_nFD(nFD)
        , m_nID(Connection::m_nConnectionCounter)
        {
            m_pBuffer = BufferManager::GetInstance()->GetBuffer(nFD);
        }

        ~Connection()
        {
            m_pBuffer->Reset();
        }

        void Send()
        {

        }

        void MarkViolation()
        {

        }
    };

    uint64_t Connection::m_nConnectionCounter = 1;
}
#endif	/* CONNECTION_H */

