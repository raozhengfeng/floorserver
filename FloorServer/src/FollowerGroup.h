/*
 * File:   FollowerGroup.h
 * Author: raozf
 *
 * Created on 2014年9月4日, 下午4:49
 */

#ifndef FOLLOWERGROUP_H
#define	FOLLOWERGROUP_H

#include "Object.h"
#include "Follower.h"
#include "ConnectionManager.h"
namespace Floor
{

    class FollowerGroup
    : public Object
    , public Runable
    {
    private:
        int m_nEpollFD;
        size_t m_nFollowers;
        std::vector<Follower*> m_Followers;
        ConnectionManager* m_pConnectionManager;

        ConnectCallback m_ConnectCallback;
        RecvCallback m_RecvCallback;
    protected:
        virtual bool t_Create()
        {
            //epoll
            m_nEpollFD = epoll_create(256);
            if (m_nEpollFD == -1)
            {
                LOG(LOG_LEVEL_ERROR, "epoll_create() failed. " << strerror(errno));
                goto _ERROR;
            }

            //connection manager
            m_pConnectionManager = new ConnectionManager();
            if (m_pConnectionManager == NULL)
            {
                LOG(LOG_LEVEL_FATAL, "Out of memory! New ConnectionManager failed.");
                goto _ERROR;
            }

            if(m_pConnectionManager->Create() == false)
            {
                LOG(LOG_LEVEL_FATAL, "ConnectionManager " << _p <<" create() failed.");
                goto _ERROR;
            }

            //followers
            for (size_t i = 0; i < m_nFollowers; i++)
            {
                Follower* _p = new Follower("Follower", m_nEpollFD, m_RecvCallback);
                if (_p == NULL)
                {
                    LOG(LOG_LEVEL_FATAL, "Out of memory! New Follower failed.");
                    goto _ERROR;
                }
                m_Followers.push_back(_p);

                if (_p->Create() == false)
                {
                    LOG(LOG_LEVEL_FATAL, "Follower " << _p <<" create() failed.");
                    goto _ERROR;
                }
            }
            return true;

_ERROR:
            t_Destory();
            return false;
        }

        virtual void t_Destory()
        {
            CLOSE_SOCKET(m_nEpollFD);

            SAFE_DESTORY(m_pConnectionManager);
            BOOST_FOREACH(Follower* _pFollower, m_Followers)
            {
                SAFE_DESTORY(_pFollower);
            }
            m_Followers.clear();
        }

        virtual bool t_Start()
        {
            BOOST_FOREACH(Follower* _pFollower, m_Followers)
            {
                if (_pFollower != NULL)
                {
                    if (_pFollower->Start() == false)
                    {
                        LOG(LOG_LEVEL_FATAL, "Follower (" << _pFollower << ") Start() failed.");
                        goto _ERROR;
                    }
                }
            }
            return true;

_ERROR:
            t_Stop();
            return false;
        }

        virtual void t_Stop()
        {
            BOOST_FOREACH(Follower* _pFollower, m_Followers)
            {
                if (_pFollower != NULL)
                {
                    _pFollower->Stop();
                }
            }
        }

    public:
        FollowerGroup(const std::string& strName,
                size_t nFollowers,
                ConnectCallback OnConnectCallback,
                RecvCallback OnRecvCallback)
        : Object(strName)
        , Runable(strName)
        , m_nFollowers(nFollowers)
        , m_ConnectCallback(OnConnectCallback)
        , m_RecvCallback(OnRecvCallback)
        , m_pConnectionManager(NULL)
        , m_nEpollFD(-1)
        {
            ASSERT(nFollowers > 0);
        }

        virtual ~FollowerGroup() { }

        void DispatchConnection(int nSocket)
        {
            if (m_nEpollFD > 0)
            {
                //allocate connection
                //m_pConnectionManager->

                //add to epoll listen queue
                struct epoll_event _Events;
                _Events.data.fd = nSocket;
                _Events.events = EPOLLIN | EPOLLOUT | EPOLLONESHOT;

                epoll_ctl(m_nEpollFD, EPOLL_CTL_ADD, nSocket, &_Events);

                //user specified callback
                //if(m_ConnectCallback->empty() )
                {
                    m_ConnectCallback(NULL);
                }
            }
        }
    };

}
#endif	/* FOLLOWERGROUP_H */

