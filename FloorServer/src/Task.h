/* 
 * File:   Task.h
 * Author: raozf
 *
 * Created on 2014年8月22日, 上午11:39
 */

#ifndef TASK_H
#define	TASK_H

#include "Runable.h"
#include "Common.h"
namespace Floor
{

    class Task
    : public boost::noncopyable
    , public Runable
    {
    protected:
        bool m_bExit;
        virtual void t_Run() = 0;

    private:
        typedef boost::shared_ptr<boost::thread> ThreadPtr;
        ThreadPtr m_pThreadObject;

    protected:

        virtual bool t_Start()
        {
            //typeid(*this).name() returned 'unmanaged' name, and it's compiler implementtion depended
            //LOG(LOG_LEVEL_NODE, "[Thread]Starting " << typeid(*this).name() << "(" << this << ")...");
            LOG(LOG_LEVEL_NODE, "[Thread]Starting " << m_strName << "(" << this << ")...");

            m_bExit = false;
            m_pThreadObject = ThreadPtr(new boost::thread(&Task::t_Run, this));
            if (m_pThreadObject.get() == NULL)
            {
                LOG(LOG_LEVEL_FATAL, "[Thread]" << m_strName << "(" << this << ") start failed.");
                return false;
            }

            LOG(LOG_LEVEL_NODE, "[Thread]" << m_strName << "(" << this << ") started.");
            return true;
        }

        virtual void t_Stop()
        {
            LOG(LOG_LEVEL_NODE, "[Thread]Stopping " << m_strName << "(" << this << ")...");

            m_bExit = true;
            if (m_pThreadObject && m_pThreadObject->joinable())
            {
                LOG(LOG_LEVEL_DEBUG, "[Thread]Joining " << m_strName << "(" << this << ")...");
                m_pThreadObject->join();
                m_pThreadObject.reset();
                LOG(LOG_LEVEL_DEBUG, "[Thread]" << m_strName << "(" << this << ") joined.");
            }

            LOG(LOG_LEVEL_NODE, "[Thread]" << m_strName << "(" << this << ") stopped.");
        }

    public:

        Task(const std::string& strName)
        : Runable(strName)
        , m_bExit(false) { }

        virtual ~Task() { }
    };

}
#endif	/* TASK_H */