/*
 * File: Server.h
 * Author: raozf
 *
 * Created on 2013年3月27日, 下午6:28
 */

#ifndef SERVER_H
#define SERVER_H

#include "Common.h"
#include "Configure.h"
#include "BanEnginee.h"
#include "SourceEnginee.h"

namespace Floor
{

    class Server
    : public Object
    , public Runable
    {
    private:
        int m_nServerPort;
        ServerConfigure* m_pConfigure;
        ConnectCallback m_ConnectCallback;
        RecvCallback m_RecvCallback;

        SourceEnginee* m_pSourceEnginee;
        BanEnginee* m_pBanEnginee;

        Server(const std::string& strName,
            uint16_t nServerPort,
            ServerConfigure* pServerConfigure,
            ConnectCallback OnConnectCallback,
            RecvCallback OnRecvCallback)
        : Object(strName)
        , Runable(strName)
        , m_nServerPort(nServerPort)
        , m_pConfigure(pServerConfigure)
        , m_ConnectCallback(OnConnectCallback)
        , m_RecvCallback(OnRecvCallback)
        , m_pSourceEnginee(NULL)
        , m_pBanEnginee(NULL)
        {
            ASSERT(m_pConfigure);
        }

        virtual ~Server() { }

    protected:
        virtual bool t_Create()
        {
            ASSERT(m_pSourceEnginee == NULL);
            ASSERT(m_pBanEnginee == NULL);

            //create source enginee
            m_pSourceEnginee = new SourceEnginee("SourceEnginee",
                m_nServerPort,
                &m_pConfigure->m_SourceEngineeConfigure,
                m_ConnectCallback,
                m_RecvCallback);
            if (m_pSourceEnginee == NULL)
            {
                LOG(LOG_LEVEL_FATAL, "Out of memory! New SourceEnginee failed.");
                goto _ERROR;
            }

            if (m_pSourceEnginee->Create() == false)
            {
                LOG(LOG_LEVEL_FATAL, "SourceEnginee Create() failed.");
                goto _ERROR;
            }

            //create ban enginee
            m_pBanEnginee = new BanEnginee();
            if (m_pBanEnginee == NULL)
            {
                LOG(LOG_LEVEL_FATAL, "Out of memory! New BanEnginee failed.");
                goto _ERROR;
            }

            return true;
_ERROR:
            t_Destory();
            return false;
        }

        virtual void t_Destory()
        {
            SAFE_DESTORY(m_pSourceEnginee);
            SAFE_DELETE(m_pBanEnginee);
        }

        virtual bool t_Start()
        {
            ASSERT(m_pSourceEnginee);
            ASSERT(m_pBanEnginee);

            m_pBanEnginee->Start();
            m_pSourceEnginee->Start();
        }

        virtual void t_Stop()
        {
            ASSERT(m_pSourceEnginee);
            ASSERT(m_pBanEnginee);

            m_pSourceEnginee->Stop();
            m_pBanEnginee->Stop();
        }

    public:
        static Server* NewServer(uint16_t nSreverPort,
                ServerConfigure* pServerConfigure,
                ConnectCallback OnConnectCallback,
                RecvCallback OnRecvCallback)
        {
            Server* _p = new Server("Server", nSreverPort, pServerConfigure, OnConnectCallback, OnRecvCallback);
            if (_p == NULL)
            {
                LOG(LOG_LEVEL_FATAL, "Out of memory! New Server failed.");
                goto _ERROR;
            }

            if (_p->Create() == false)
            {
                goto _ERROR;
            }

            return _p;

_ERROR:
            SAFE_DELETE(_p);
            return NULL;
        }

        static void DestoryServer(Server* pServer)
        {
            if (pServer == NULL)
            {
                return;
            }

            pServer->Destory();
            SAFE_DELETE(pServer);
        }
    };

}
#endif /* SERVER_H */