/* 
 * File:   Configure.h
 * Author: raozf
 *
 * Created on 2014年8月22日, 下午2:17
 */

#ifndef CONFIGURE_H
#define	CONFIGURE_H
namespace Floor
{

    typedef struct
    {
        //follower configure
        uint16_t m_nFollowerGroups; //number of epoll_wait() gorups
        uint16_t m_nFollowersPerGroup; //number of workers per epoll_wait() group

        //socket configure
        uint16_t m_nSocketInactiveTimeout; //
        uint16_t m_nMaxMsgLen; //

        //epoll
        uint16_t m_nListenerMaxEvents; //
    } SourceEngineeConfigure;

    typedef struct
    {
        bool m_bEnableBanEnginee;
        uint16_t m_nMaxPermits; //
        uint32_t m_nMaxMsgPerSecond; //
        uint32_t m_nMaxMsgPerMinute; //
        uint32_t m_nMaxMsgPerHour; //    
    } BanEngineeConfigure;

    typedef struct
    {
    } BusinessEngineeConfigure;

    typedef struct
    {
        SourceEngineeConfigure m_SourceEngineeConfigure;
        BanEngineeConfigure m_BanEngineeConfigure;
        BusinessEngineeConfigure m_BussinessEngineeConfigure;

        bool Load(const std::string& strConfigur) {
        }
    } ServerConfigure;

}
#endif	/* CONFIGURE_H */

