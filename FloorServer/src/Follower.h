/*
 * File: Follower.h
 * Author: chu
 *
 * Created on 2013年3月9日, 下午4:56
 */

#ifndef FOLLOWER_H
#define FOLLOWER_H

#include "FloorServer.h"
#include "Common.h"
#include "Object.h"
#include "Task.h"
#include "ISendable.h"
#include "Test.h"
#include "BufferManager.h"
namespace Floor
{

    class Follower
    : public Object
    , public Task
    , public ISendable
    {
    private:
        RecvCallback m_RecvCallback;
        int m_nEpollFD;
        int m_nCurrentFD;

        BufferManager* m_pBufferManager;
        FollowerBuffer* m_pBuffer;

    private:
        void m_HandleReadEvent()
        {
            m_pBuffer = m_pBufferManager->GetBuffer(m_nCurrentFD);
            if(m_pBuffer == NULL)
            {
                return;
            }

            int _nRet = 0;
            if(m_pBuffer->m_nHeaderIndex < 4)//recv header
            {
                _nRet = m_Read(m_pBuffer->m_szHeaderBuf + m_pBuffer->m_nHeaderIndex, 4 - m_pBuffer->m_nHeaderIndex);
                if (_nRet <= 0)
                {//no data:no need to close socket; or socket closed already
                    return;
                }

                m_pBuffer->m_nHeaderIndex += _nRet;
                if (m_pBuffer->m_nHeaderIndex < 4)
                {//read part of data:need to read next time
                    LOG(LOG_LEVEL_WARN, "Read header, got " << _nRet << " bytes, excepted "
                        << 4 - m_pBuffer->m_nHeaderIndex <<" bytes. Need to read next time.");
                    return;
                }
                //HexDump(m_pBuffer->m_szHeaderBuf, 4);
                //read heder ok

                //parse header
                size_t* _p = (size_t*) m_pBuffer->m_szHeaderBuf;
                m_pBuffer->m_nRecvLength = (size_t) * _p;
                LOG(LOG_LEVEL_DEBUG, "payload length:" << m_pBuffer->m_nRecvLength);
                if (m_pBuffer->m_nRecvLength >= MAX_RECV_BUF_LEN)
                {
                    LOG(LOG_LEVEL_DEBUG, "Invalid header value. got " << m_pBuffer->m_nRecvLength << ".");
                    goto _ERROR;
                }
            }

            //recv payload
            _nRet = m_Read(m_pBuffer->m_szRecvBuf, m_pBuffer->m_nRecvLength - m_pBuffer->m_nRecvIndex);
            if(_nRet <= 0)
            {
                return;
            }

            m_pBuffer->m_nRecvIndex += _nRet;
            if(m_pBuffer->m_nRecvIndex < m_pBuffer->m_nRecvLength)
            {//read part of data:need to read next time
                LOG(LOG_LEVEL_WARN, "Read payload, got " << _nRet << " bytes, excepted "
                    << m_pBuffer->m_nRecvLength - m_pBuffer->m_nRecvIndex <<" bytes. Need to read next time.");
                return;
            }
            //HexDump(m_szRecvBuf, m_nMsgLen);
            //read payload ok

            //handle msg
            m_RecvCallback(m_pBuffer->m_szRecvBuf, m_pBuffer->m_nRecvLength, this);
            m_pBuffer->Reset();
            return;

_ERROR:
            m_pBuffer->Reset();
            CLOSE_SOCKET(m_nCurrentFD);
        }

        // -1: error, socket closed
        // [0, nExpectedBytes) : read part of datagram,need to read again--socket keep open
        // nExpectedBytes: ok
        int m_Read(char* pBuf, size_t nExpectedBytes)
        {
            int _nRet = -1;
            int _nReceived = 0;
            char* _p = pBuf;

            while (!m_bExit)
            {
                _nRet = recv(m_nCurrentFD, _p, nExpectedBytes - _nReceived, 0);
                if (_nRet < 0)
                {
                    if (errno == EAGAIN)
                    {
                        //no data to read on socket when nonblock. read finished.
                        LOG(LOG_LEVEL_DEBUG, "[fd:" << m_nCurrentFD << "]no data.");
                        //no need to close socket
                    }
                    else//error
                    {
                        LOG(LOG_LEVEL_DEBUG, "[fd:" << m_nCurrentFD << "] recv() error, returned :" << _nRet << ". error: " << strerror(errno));
                        CLOSE_SOCKET(m_nCurrentFD);
                        _nReceived = -1;
                    }
                    break;
                }
                else if (_nRet == 0)//peer point socket is closed
                {
                    LOG(LOG_LEVEL_INFO, "[fd:" << m_nCurrentFD << "] closed by peer.");
                    CLOSE_SOCKET(m_nCurrentFD);
                    _nReceived = -1;
                    break;
                }
                else
                {
                    _nReceived += _nRet;
                    _p = pBuf + _nRet;
                    if (_nReceived >= nExpectedBytes)
                    {
                        LOG(LOG_LEVEL_DEBUG, "[fd:" << m_nCurrentFD << "] read " << _nReceived << " bytes, excepted:" << nExpectedBytes << " bytes.");
                        break;
                    }
                    /*
                     else //continue to read
                    {
                        continue;
                    }
                     */
                }
            }

            return _nReceived;
        }

        void m_HandleWriteEvent()
        {
            int _nRet = 0;
            while(m_pBuffer->m_nSendIndex < m_pBuffer->m_nSendLength)
            {
                 _nRet = send(m_nCurrentFD,
                    m_pBuffer->m_szSendBuf + m_pBuffer->m_nSendIndex,
                    m_pBuffer->m_nSendLength - m_pBuffer->m_nSendIndex,
                    0);
                if(_nRet == -1)
                {
                    if (errno != EINTR && errno != EAGAIN)
                    {//error
                        LOG(LOG_LEVEL_ERROR, "Send failed! "<< strerror(errno));
                        CLOSE_SOCKET(m_nCurrentFD);
                    }
                    return ;
                }
                else
                {
                    m_pBuffer->m_nSendIndex += _nRet;
                }
            }
        }

    protected:
        virtual bool t_Create()
        {
            m_pBufferManager = BufferManager::GetInstance();
            return true;
        }

        virtual void t_Destory() { }

        virtual void t_Run()
        {
            LOG(LOG_LEVEL_DEBUG, "Follower::t_Run().");

            int _nEvents = 0;
            struct epoll_event _Ev;
            struct epoll_event _EventsRegistered;

            while (m_bExit == false)
            {
                _nEvents = epoll_wait(m_nEpollFD, &_Ev, 1, 1000);
                if (_nEvents > 0)
                {
                    m_nCurrentFD = _Ev.data.fd;

                    if (_Ev.events & EPOLLIN)
                    {
                        m_HandleReadEvent();
                    }
                    else if (_Ev.events & EPOLLOUT)
                    {
                        m_HandleWriteEvent();
                    }

                    _EventsRegistered.data.fd = m_nCurrentFD;
                    _EventsRegistered.events = EPOLLIN | EPOLLONESHOT;
                    if (m_pBuffer->m_nSendIndex > 0)//some data to be sent
                    {
                        _EventsRegistered.events |= EPOLLOUT;
                    }
                    epoll_ctl(m_nEpollFD, EPOLL_CTL_MOD, m_nCurrentFD, &_EventsRegistered);
                }
            }

            LOG(LOG_LEVEL_DEBUG, "Follower::t_Run() exited.");
        }

    public:
        Follower(const std::string & strName, int nEpollFD, RecvCallback callback)
        : Object(strName)
        , Task(strName)
        , m_nEpollFD(nEpollFD)
        , m_RecvCallback(callback)
        {
            ASSERT(m_nEpollFD > 0);
        }

        virtual ~Follower() { }

        virtual int Send(char* pData, size_t nLen)
        {
            LOG(LOG_LEVEL_DEBUG, "sending...");

            if(nLen > MAX_SEND_BUF_LEN)
            {
                LOG(LOG_LEVEL_ERROR, "No sent! msg too long. nLen:" <<nLen <<", max length:" << MAX_SEND_BUF_LEN);
                return -1;
            }

            memcpy(m_pBuffer->m_szSendBuf, pData, nLen);
            m_pBuffer->m_nSendLength = nLen;
            m_HandleWriteEvent();
            return 0;
        }
    };


}
#endif /* FOLLOWER_H */
