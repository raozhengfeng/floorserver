/* 
 * File:   BufferManager.h
 * Author: raozf
 *
 * Created on 2014年9月9日, 上午11:22
 */

#ifndef BUFFERMANAGER_H
#define	BUFFERMANAGER_H

#include "Common.h"
#include "ISendable.h"
namespace Floor
{
    
    typedef struct __FOLLOWER_BUFFER
    {
        __FOLLOWER_BUFFER()
        {
            Reset();
        }
        
        void Reset()
        {
            m_nHeaderIndex = 0;
            m_nRecvIndex = 0;
            m_nSendIndex = 0;
            m_nRecvLength = 0;
            m_nSendLength = 0;
            
            memset(m_szHeaderBuf, 0, 4);
            memset(m_szRecvBuf, 0, MAX_RECV_BUF_LEN);
            memset(m_szSendBuf, 0, MAX_SEND_BUF_LEN);
        }
        
        unsigned short m_nHeaderIndex;
        unsigned short m_nRecvIndex;
        unsigned short m_nSendIndex;
        size_t m_nRecvLength;
        size_t m_nSendLength;
        char m_szHeaderBuf[4];                                          //msg header(msg length), 4 bytes
        char m_szRecvBuf[MAX_RECV_BUF_LEN];              //msg payload(for recv)
        char m_szSendBuf[MAX_SEND_BUF_LEN];             //msg payload(for send)
    }FollowerBuffer;
    
    class BufferManager
        : public boost::noncopyable
    {
    private:
        std::map<int, FollowerBuffer*> m_BufferMap;
        boost::mutex m_Mutex;
        
    private:
        BufferManager(){}
        ~BufferManager(){}
                
    public:
        static BufferManager* GetInstance()
        {
            static BufferManager _instance;
            return &_instance;
        }
        
        // leader-follower mode means a connection will be processed by only one thread,
        // so we just need to lock if new buffer added to the map
        FollowerBuffer* GetBuffer(int nFD)
        {
            std::map<int, FollowerBuffer*>::iterator _it = m_BufferMap.find(nFD);
            if(_it != m_BufferMap.end())
            {
                return _it->second;
            }
            
            FollowerBuffer* _p = new FollowerBuffer();
            if(_p != NULL)
            {
                LOCK _l(m_Mutex);
                m_BufferMap[nFD] = _p;
            }
            else
            {
                LOG(LOG_LEVEL_ERROR, "Out of memory! new FollowerBuffer failed.");
            }
            
            return _p;
        }
    };
    
    
}
#endif	/* BUFFERMANAGER_H */
