#include <boost/function.hpp>
#include "ISendable.h"
namespace Floor
{

    typedef boost::function<void(char*, size_t, ISendable* pSender) > RecvCallback;
    typedef boost::function<void(ISendable* pSender) > ConnectCallback;

    //helper function, for app use
    //will block forever, until server exited
    int StartFloorServer(int argc, char** argv,
            RecvCallback OnRecvCallback,
            ConnectCallback OnConnectCallback,
            bool bDaemonized,
            bool bStartMonitorProcess);

}