/* 
 * File:   ISendable.h
 * Author: raozf
 *
 * Created on 2014年9月5日, 下午4:36
 */

#ifndef ISENDABLE_H
#define	ISENDABLE_H
namespace Floor
{

    #define MAX_SEND_BUF_LEN 4*1024
    class ISendable
    {
    public:
        ISendable(){}
        
        virtual ~ISendable(){}
        
        virtual int Send(char* pData, size_t nLen) = 0;
    };

}
#endif	/* ISENDABLE_H */
