#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/890770006/main.o \
	${OBJECTDIR}/_ext/1360937237/FloorServer.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ../lib/${CND_CONF}/${CND_PLATFORM}/libfloorserver.a

../lib/${CND_CONF}/${CND_PLATFORM}/libfloorserver.a: ${OBJECTFILES}
	${MKDIR} -p ../lib/${CND_CONF}/${CND_PLATFORM}
	${RM} ../lib/${CND_CONF}/${CND_PLATFORM}/libfloorserver.a
	${AR} -rv ../lib/${CND_CONF}/${CND_PLATFORM}/libfloorserver.a ${OBJECTFILES} 
	$(RANLIB) ../lib/${CND_CONF}/${CND_PLATFORM}/libfloorserver.a

${OBJECTDIR}/_ext/890770006/main.o: ../../Sample/echo/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/890770006
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/890770006/main.o ../../Sample/echo/main.cpp

${OBJECTDIR}/_ext/1360937237/FloorServer.o: ../src/FloorServer.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1360937237
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1360937237/FloorServer.o ../src/FloorServer.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ../lib/${CND_CONF}/${CND_PLATFORM}/libfloorserver.a

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
