#include <boost/bind.hpp>
#include <iostream>
#include "FloorServer.h"

void OnConnect(Floor::ISendable* pSender)
{
    std::cout<<"hahaha...................."<<std::endl;
}

/*
 * echo server.
 * sample app, show how to use FloorServer
 *
 * WARN: you can  use param 'pSender' in this function ONLY, that means you can't save this param and call it in any otherwhere.
 */
void OnRecv(char* pData, size_t nLen, Floor::ISendable* pSender)
{
	//TODO:if data invalid, mark socket as voilate security rules
    pSender->Send(pData, nLen);
}

int main(int argc, char** argv)
{
    //TODO:init

    Floor::StartFloorServer(argc, argv,
        boost::bind(&OnRecv, _1, _2, _3),
        boost::bind(&OnConnect, _1),
        false,
        false);
}
